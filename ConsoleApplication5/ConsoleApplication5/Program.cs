﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    struct BigNumber
    {
        public int[] a;
        public int size;

        public BigNumber(string s)
        {
            a = new int[1000];
            size = s.Length;
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }

        public static BigNumber operator +(BigNumber arg1, BigNumber arg2)
        {
            BigNumber r = new BigNumber("");
            int NewSize = Math.Max(arg1.size, arg2.size); 
            for (int i = 0; i < NewSize; i++)
            {
                r.a[i] = arg1.a[i] + arg2.a[i];
                if (r.a[i] > 9)
                {
                    r.a[i] = r.a[i] % 10;
                    arg1.a[i + 1] += 1;

                    if (i == NewSize - 1)
                        NewSize++;
                }
                r.size++;
            }

            return r;
        }

        public override string ToString()
        {
            string s = "";
            for (int i = size - 1; i >= 0; i--)
            {
                s = s + a[i].ToString();
            }
            return s;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            BigNumber a = new BigNumber("999999");
            BigNumber b = new BigNumber("1");
            BigNumber c = a + b;
            Console.WriteLine(c);
            Console.ReadLine();
        }
    }
}
