﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    struct DNumber
    {
        public int a, b;
        public DNumber(int x, int y)
        {
            a = x;
            b = y;
        }
        public override string ToString()
        {
            return a.ToString() + "/" + b.ToString();
        }
        public void normize()
        {
            int x = Math.Abs(a);
            int y = Math.Abs(b);
            while (x > 0 && y > 0)
            {
                if (x > y)
                    x = x % y;
                else
                    y = y % x;
            }
            x = x + y;
            a /= x;
            b /= x;
        }
        public static DNumber operator *(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.a;
            arg1.b *= arg2.b;
            arg1.normize();
            return arg1;
        }
        public static DNumber operator /(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg1.b *= arg2.a;
            arg1.normize();
            return arg1;
        }
        public static DNumber operator +(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg2.a *= arg1.b;
            arg1.a += arg2.a;
            arg1.b *= arg2.b;
            arg1.normize();
            return arg1;
        }
        public static DNumber operator -(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg2.a *= arg1.b;
            arg1.a -= arg2.a;
            arg1.b *= arg2.b;
            arg1.normize();
            return arg1;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DNumber x = new DNumber(1, 2);
            DNumber y = new DNumber(2, 3);
            DNumber z = x * y;
            DNumber n = x / y;
            DNumber k = x + y;
            DNumber m = x - y;
            Console.WriteLine(z);
            Console.WriteLine(n);
            Console.WriteLine(k);
            Console.WriteLine(m);
            Console.ReadLine();
        }
    }
}
